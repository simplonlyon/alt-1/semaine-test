import { Request, Response } from "express";
import { Dog } from "../entity/Dog";
import { DogRepository } from "../repository/DogRepository";

export class DogController {
    
    constructor(private repo = new DogRepository()) {}

    async getAllDogs(request:Request, response:Response) {
        const dogs = await this.repo.findAll();

        response.status(200).json(dogs);
    }

    async addDog(request:Request, response:Response) {
        const dog = new Dog(
            request.body.name,
            request.body.breed,
            request.body.age,
        );
        await this.repo.add(dog);
        
        response.status(201).json(dog);

    }
}