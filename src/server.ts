import bodyParser from 'body-parser';
import express from 'express';
import { dogRouter } from './route/route-dog';

export const app = express();

app.use(bodyParser.json());

app.get('/example', (req,res) => {

    res.json({data:'coucou'});
});


app.use('/api/dog', dogRouter);