import { Router } from "express";
import { DogController } from "../controller/DogController";

const controller = new DogController();

export const dogRouter = Router();

dogRouter.get('/', controller.getAllDogs.bind(controller));
dogRouter.post('/', controller.addDog.bind(controller));