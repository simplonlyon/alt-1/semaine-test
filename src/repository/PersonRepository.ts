import { getRepository } from "typeorm";
import { Person } from "../entity/Person";


export class PersonRepository {
    
    constructor(private repo = getRepository(Person)){}

    findAll() {
        return this.repo.find();
    }

    add(person:Person) {
        return this.repo.save(person);
    }
}