
import { createConnection } from "typeorm";
import { Person } from "./entity/Person";
import { app } from "./server";
import { connectionOptions } from "./util/connection-options";


const port = process.env.PORT || 3000;


createConnection(connectionOptions).then(() => {

    app.listen(port, () => {
        console.log('listening on '+port);
    });
});