

import { ConnectionOptions } from "typeorm";
import { Person } from "../entity/Person";


export const connectionOptions:ConnectionOptions = {
    type: 'mysql',
    database: process.env.DB_NAME,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    entities: [
        Person
    ],
    synchronize:true
}