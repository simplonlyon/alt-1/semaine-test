
import supertest from 'supertest';
import { app } from '../src/server';


describe('Functional test example', () => {


    it('should display coucou on route /example', async () => {

        const client = supertest(app);

        const response = await client.get('/example');
        
        expect(response.status).toBe(200);
        expect(response.body).toStrictEqual({data:'coucou'});

    });

    

});