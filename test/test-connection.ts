import { createConnection, getRepository } from "typeorm";
import { Person } from "../src/entity/Person";


export async function testConnection() {

    await createConnection({
        type: 'sqlite',
        database: ':memory:',
        entities: [Person],
        synchronize: true,
        dropSchema: true
    });
}

//WIP: Faire les data set pour les tests qui seront à
//lancer avant chaque test
export async function fixturePerson() {
    getRepository(Person).save(new Person)
}