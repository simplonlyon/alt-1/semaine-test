import supertest, { SuperTest, Test } from "supertest";
import { connection } from "../src/repository/connection";
import { DogRepository } from "../src/repository/DogRepository";
import { app } from "../src/server";

describe('API Route for dogs', () => {

    let client:SuperTest<Test>;
    beforeEach(() => {
        client = supertest(app);
    });

    beforeEach(async () => {
        await connection.query('START TRANSACTION');
    });

    afterEach(async () => {
        await connection.query('ROLLBACK');
    });

    it('should return a list of dogs', async () => {

        const response = await client.get('/api/dog');

        expect(response.status).toBe(200);
        expect(response.body).toBeInstanceOf(Array);
        expect(response.body.length).toBe(2);

        const ExpectedDogObject = expect.objectContaining({
            id: expect.any(Number),
            name: expect.any(String),
            breed: expect.any(String),
            age: expect.any(String),
        });

        expect(response.body).toContainEqual(ExpectedDogObject);

        expect(response.body[0].name).toBe('Fido');
        expect(response.body[0].breed).toBe('Corgi');
        expect(response.body[0].age).toBe('1');

    });


    it('should add a dog on post', async () => {

        const response = await client.post('/api/dog').send({
            name: 'Test',
            breed: 'Test',
            age: '3'
        });

        expect(response.status).toBe(201);


        expect(response.body.name).toBe('Test');
        expect(response.body.breed).toBe('Test');
        expect(response.body.age).toBe('3');
        expect(response.body.id).toBeDefined();
        
        //Pas ouf
        const results = await new DogRepository().countDog();

        expect(results).toBe(3);
        
    });

});