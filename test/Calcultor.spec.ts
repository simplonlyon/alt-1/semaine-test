import { Calculator } from "../src/Calculator";



describe('class Calculator', () => {

    let instance:Calculator;

    beforeEach(() => {
        instance = new Calculator();
    });

    it('should perform addition', () => {
        
        const result = instance.calculate(2,2,'+');
        expect(result).toEqual(4);
        

    });

    it('should perform substraction', () => {
        
        const result = instance.calculate(2,2,'-');
        expect(result).toEqual(0);

    });

    it('should perform multiply', () => {
        
        const result = instance.calculate(2,3,'*');
        expect(result).toEqual(6);

    });

    it('should perform division', () => {
        
        const result = instance.calculate(2,2,'/');
        expect(result).toEqual(1);

    });

    it('should throw error when division by zero', () => {

        expect( () => instance.calculate(1, 0, '/') ).toThrowError();
    });

    it('should throw error when unknown operator', () => {

        expect( () => instance.calculate(1, 0, 'x') ).toThrowError('Invalid operator');
    });

    it('should throw error when a or b is not a number', () => {
        const a:any = 'jambon';

        expect( () => instance.calculate(a, 10, '*') ).toThrowError('a and b must be numbers');

    })

    it.each([
        [1,1,'+',2],
        [1,1,'-',0],
        [1,1,'*',1],
        [4,2,'/',2],
        [0.3,1,'+',1.3],
    ])('should perform operation with a = %d b = %d operator = %s', (a,b,operator,result) => {
        expect(instance.calculate(a,b,operator)).toEqual(result)
    })
});