import { DogController } from "../../src/controller/DogController";
import { Dog } from "../../src/entity/Dog";

describe('class DogController', () => {
    let instance:DogController;
    let fakeRepo;
    beforeEach(() => {
        fakeRepo = {
            findAll: jest.fn()
        }
        instance = new DogController(fakeRepo);
    });

//Comme on voit ça fait beaucoup de code et de mock pour tester au final pas grand chose
//je ne recommande donc pas de tester unitairement les contrôleurs 
//mais plutôt de faire en sorte d'externaliser la majorité de l'algo
//de ceci pour pouvoir la tester unitairement plus facilement
    it('should send a 200 status and list of dog on getAll', async () => {
        const dogs = [
            new Dog('test', 'test', 1, 1)
        ];
        fakeRepo.findAll.mockResolvedValue(dogs);

        const fakeRequest:any = {}
        const fakeResponse:any = {
            status: jest.fn(),
            json: jest.fn()
        }
        fakeResponse.status.mockReturnValue(fakeResponse);

        await instance.getAllDogs(fakeRequest, fakeResponse);

        expect(fakeResponse.status).toHaveBeenCalledWith(200);
        expect(fakeResponse.json).toHaveBeenCalledWith(dogs);

    });


});
