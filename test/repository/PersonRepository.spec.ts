import {  getConnection } from "typeorm";
import { Person } from "../../src/entity/Person";
import { PersonRepository } from "../../src/repository/PersonRepository";
import {testConnection} from '../test-connection'

describe('class PersonRepository', () => {


    beforeEach(async () => {
        await testConnection();
    });

    afterEach(async () => {
        await getConnection().close();
    });


    it('should add a new person', async () => {

        const repo = new PersonRepository();
        const person = new Person();
        person.lastName = 'Test';
        person.name = 'Test';
        await repo.add(person);

        const result = await repo.findAll();

        expect(result.length).toBe(1);

    });

    it('should add a new person2', async () => {

        const repo = new PersonRepository();
        const person = new Person();
        person.lastName = 'Test';
        person.name = 'Test';
        await repo.add(person);

        const result = await repo.findAll();

        expect(result.length).toBe(1);

    });

    it('should add a new person3', async () => {

        const repo = new PersonRepository();
        const person = new Person();
        person.lastName = 'Test';
        person.name = 'Test';
        await repo.add(person);

        const result = await repo.findAll();

        expect(result.length).toBe(1);

    });

});